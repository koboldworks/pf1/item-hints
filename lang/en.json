{
	"Koboldworks": {
		"ItemHint": {
			"Description": {
				"ScriptCall": "Script",
				"ScriptCallHint": "Script Call",
				"Slotless": "Free",
				"SlotlessHint": "No slot cost.",
				"SpellpointlessHint": "No spellpoint cost.",
				"SlotCost": "{cost} slots",
				"SpellpointCost": "{cost} spellpoints",
				"SpellpointCostShort": "{cost} pts"
			},
			"AuraLabel": "Display aura school and power",
			"AuraHint": "If disabled, no aura info is displayed for identified magic items.",
			"IconizeLabel": "Iconize hints",
			"IconizeHint": "Use icons instead of text for hints where possible.",
			"IconizeSchoolLabel": "Iconized aura school method",
			"IconizeSchoolHint": "Keys uses 3 letter combo. Thassilonian runes miss Divination and Universal and thus use Truth and Diligence runes instead.",
			"ValueLabel": "Show values",
			"ValueHint": "Show value for different selections of items.",
			"Value": {
				"TradeGoodsOnly": "Trade goods only",
				"TradeGoodsAndContainers": "Trade goods & containers",
				"Expanded": "Trade goods, containers, and container contents",
				"All": "Everything",
				"None": "None"
			},
			"Icons": {
				"Letters": "Keys",
				"LettersWithCL": "Keys with CL",
				"Runes": "Thassilonian",
				"Custom": "Game Icons"
			},
			"SanityLabel": "Warn about odd values",
			"SanityHint": "Adds warnings about strange values among the hints.",
			"ChangeLabel": "Show Change targets",
			"ChangeHint": "These are only guidelines and not entirely reliable or accurate.",
			"LangLabel": "Include non-standard labels.",
			"LangHint": "Includes shorter labels introduced by the module not present in base system. Has effect only if non-English language is used.",
			"MaxLabelLength": "Max Label Length",
			"MaxLabelLengthHint": "If change/note hint's label grows longer than this, it'll be replaced with simpler \"Many\" tag.",
			"CompatLabel": "Compatibility mode",
			"CompatHint": "Enable compatibility mode, ignoring some errors known to cause issues with alternate character sheets.",
			"HomebrewLabel": "Homebrew support",
			"HomebrewHint": "Toggles some homebrew supporting functionality. For now this simply disables item caster level check.",
			"CustomHints": "Custom Hints",
			"CustomPlaceholder": "No custom hints. Read above for instructions.",
			"ExampleOutput": "Example Output",
			"Instructions": "Custom item hints can be used to show changing information about an item. Semicolon (;) can be used to separate multiple hints. Vertical bar (|) can be used to separate text from hover (tooltip) text and from additional instructions (format: label|hint|instructions). Roll data is available for inline rolls."
		},
		"Hints": {
			"Many": "Many",
			"Enhancement": "+{enh} Enhancement",
			"ValueMult": "{value} × {quantity} = {total}",
			"ContentsValue": "{value} (contents: {contents})",
			"Contents": "{quantity} items",
			"ContentsIndefinite": "{quantity}+ items",
			"Contextual": "Contextual"
		},
		"Generic": {
			"Unknown": "Unknown",
			"NoAction": "No Action",
			"NoAmmoType": "No Ammo Type",
			"Secondary": "Secondary",
			"Nonlethal": "Nonlethal",
			"UnknownSource": "Unknown",
			"BadSource": "BROKEN"
		},
		"Sanity": {
			"FCBInequal": "FCB not equal to level (difference of {diff}).",
			"InsufficientCL": "Insufficient caster level (difference of {diff}).",
			"Change": {
				"TargetlessLabel": "No Target",
				"TargetlessHint": "Includes an incomplete Change or Context Note with no target.",
				"MissingSkillLabel": "Bad Skill",
				"MissingSkillHint": "Custom skill not found: {skillId}",
				"BadTargetLabel": "Bad Target",
				"BadTargetHint": "Includes Change or Context Note with malformed target: {target}",
				"FormulalessLabel": "No Formula",
				"FormulalessHint": "Includes an incomplete Change with no formula.",
				"TextlessLabel": "No Text",
				"TextlessHint": "Includes an incomplete Context Note with no text: {target}."
			}
		},
		"Short": {
			"AC": "AC",
			"CMD": "CMD",
			"Attack": "ATK",
			"Damage": "DMG",
			"Skill": "Skill",
			"Spell": "Spell",
			"Misc": "Misc",
			"Health": "Health",
			"Defense": "Defense",
			"SpellDC": "Spell DC",
			"DexterityToAC": "DexToAC",
			"Speed": "Spd",
			"SavingThrow": "Save",
			"Ability": "Abl",
			"NaturalAC": "Natural AC",
			"TouchAC": "Touch AC",
			"ArmorMaxDex": "Max Dex (Armor)",
			"ShieldMaxDex": "Max Dex (Shield)",
			"FlatfootedAC": "FF AC",
			"FlatfootedCMD": "FF CMD",
			"CarryMult": "Carry Mult.",
			"StrSkills": "Str Skills",
			"DexSkills": "Dex Skills",
			"ConSkills": "Con Skills",
			"IntSkills": "Int Skills",
			"WisSkills": "Wis Skills",
			"ChaSkills": "Cha Skills",
			"SkillRanks": "Skill Ranks",
			"StrChecks": "Str Checks",
			"DexChecks": "Dex Checks",
			"ConChecks": "Con Checks",
			"IntChecks": "Int Checks",
			"WisChecks": "Wis Checks",
			"ChaChecks": "Cha Checks",
			"StrMod": "Str Mod",
			"DexMod": "Dex Mod",
			"ConMod": "Con Mod",
			"IntMod": "Int Mod",
			"WisMod": "Wis Mod",
			"ChaMod": "Cha Mod",
			"MeleeWeapon": "Melee Weapon",
			"RangedWeapon": "Ranged Weapon",
			"ThrownWeapon": "Thrown Weapon",
			"Melee": "Melee",
			"Ranged": "Ranged",
			"Natural": "Natural",
			"AllAblChecks": "Ability Checks",
			"SpellResistance": "SR",
			"Initiative": "Init",
			"Reflex": "Ref",
			"Fortitude": "Fort",
			"Will": "Will",
			"AllSaves": "All Saves"
		},
		"School": {
			"odd": "Odd",
			"abj": "Abj",
			"con": "Con",
			"div": "Div",
			"enc": "Enc",
			"evo": "Evo",
			"ill": "Ill",
			"nec": "Nec",
			"trs": "Trs",
			"uni": "Uni",
			"Unknown": "???"
		},
		"Module": {
			"GMNotes": {
				"NotesFound": "GM Notes found."
			}
		},
		"Missing": {
			"PF1": {
				"Nonproficient": "Nonproficient",
				"ConcentrationAbbr": "Conc."
			}
		}
	}
}
