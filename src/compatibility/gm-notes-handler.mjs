import { Hint } from '../hint.mjs';
import { itemHandlers } from '../common.mjs';
import { i18n } from '../i18n.mjs';

function gmNotesHandler(options, hints) {
	if (!game.user.isGM) return;
	const item = options.item;

	const notes = item.getFlag('gm-notes', 'notes');
	if (notes === undefined) return;

	hints.append(
		Hint.create(i18n.get('GMNote.label'), ['module', 'gm-notes'],
			{ hint: i18n.get('Koboldworks.Module.GMNotes.NotesFound'), icon: 'fas fa-clipboard-check' },
		).element(options.iconize));
}

itemHandlers.push(gmNotesHandler);
