import { Hint } from '../hint.mjs';
import { i18n } from '../i18n.mjs';

/**
 * @param {HandlerOptions} o Options
 * @param hints
 */
export function handleAttack(o, hints) {
	const itemData = o.itemData;
	const fa = o.defaultAction;
	if (!fa) return;

	if (itemData.subType === 'natural') {
		if (!fa?.naturalAttack?.primary)
			hints.append(Hint.create(i18n.get('Koboldworks.Generic.Secondary'), ['natural', 'secondary']).element());
	}

	if (['mcman', 'rcman'].includes(fa.actionType)) {
		hints.append(Hint.create(i18n.get('PF1.CMBAbbr'), ['combat-maneuver']).element());
	}
}
