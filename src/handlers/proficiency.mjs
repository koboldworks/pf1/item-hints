import { Hint } from '../hint.mjs';
import { i18n } from '../i18n.mjs';

/**
 * @param {HandlerOptions} o
 * @param {Hint[]} hints
 */
export function handleProficiency(o, hints) {
	const actor = o.actor;
	if (!actor) return;

	const item = o.item;
	if (!['equipment', 'attack', 'weapon', 'equipment'].includes(item.type)) return;
	if (item.type === 'equipment' && !['armor', 'shield'].includes(item.subType)) return;

	if (item.type === 'attack' && item.subType !== 'weapon') return; // Non-weapons have no proficiency

	let proficient = true;
	try {
		if (item.type === 'equipment') proficient = actor.hasArmorProficiency(item);
		else {
			const override = item.type === 'attack'; // Attacks are sad little things still
			proficient = actor.hasWeaponProficiency(item, { override });
		}
	}
	catch (err) {
		// Proficiency not supported
		return;
	}

	if (proficient) return;

	hints.append(Hint.create(
		i18n.get('Koboldworks.Missing.PF1.Nonproficient'),
		['nonproficient'],
		{ icon: 'fas fa-book-dead' },
	).element(o.iconize));
}
