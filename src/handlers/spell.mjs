import { CFG } from '../config.mjs';
import { createNode, descriptorIcons } from '../common.mjs';
import { Hint } from '../hint.mjs';
import { i18n } from '../i18n.mjs';

/**
 * @param {import("../handler-options.mjs").HandlerOptions} o Options
 * @param {Element} hints hints element
 */
export const handleSpell = async (o, hints) => {
	const itemData = o.itemData,
		materials = itemData.materials,
		material = materials.value;

	const nEl = Array.from(o.element.getElementsByClassName('spell-components')).at(-1);
	// Materials use special hint
	if (material && materials.gpValue > 0 && nEl) {
		const mCost = descriptorIcons.material.clone();
		const mHints = createNode('div', null, [CFG.CSS.branding, CFG.CSS.common]);
		mHints.append(mCost.element(true));
		nEl.append(mHints);
	}

	const atWill = itemData.atWill ?? false;

	const spellbook = o.item.spellbook ?? {};

	const isBasicPrepared = spellbook.spellPreparationMode === 'prepared';
	// const preparedCaster = isBasicPrepared || spellbook.spellPreparationMode === 'hybrid';
	// const spellIsPrepared = (itemData.preparation?.value || 0) > 0;

	const usesSpellpoints = spellbook.spellPoints?.useSystem ?? false;

	/** @type {pf1.components.ItemAction} */
	const action = o.item.defaultAction;
	const actionCost = !atWill ? await action?.getChargeCost({ interactive: false, maximize: true }) : 0,
		castCost = !atWill ? (actionCost?.total ?? 0) : 0;

	if (atWill || castCost == 0) {
		const label = i18n.get('Koboldworks.ItemHint.Description.Slotless'),
			hint = !usesSpellpoints ? i18n.get('Koboldworks.ItemHint.Description.SlotlessHint') : i18n.get('Koboldworks.ItemHint.Description.SpellpointlessHint');
		const slHint = Hint.create(label, ['slotcost', 'free'], { icon: 'fas fa-infinity', hint });
		hints.append(slHint.element(o.iconize));
	}
	else if (usesSpellpoints) {
		const slHint = Hint.create(i18n.get('Koboldworks.ItemHint.Description.SpellpointCostShort', { cost: castCost, hint: i18n.get('Koboldworks.ItemHint.Description.SpellpointCost', { cost: castCost }) }), ['pointcost']);
		hints.append(slHint.element(false));
	}
	else if (!isBasicPrepared && castCost > 1) {
		const slHint = Hint.create(i18n.get('Koboldworks.ItemHint.Description.SlotCost', { cost: castCost }), ['castcost']);
		hints.append(slHint.element(false));
	}

	if (!usesSpellpoints && isBasicPrepared) {
		const slotCost = itemData.slotCost;
		if (slotCost > 1) {
			const slHint = Hint.create(i18n.get('Koboldworks.ItemHint.Description.SlotCost', { cost: slotCost }), ['slotcost']);
			hints.append(slHint.element(false));
		}
	}

	if (action?.hasSave) {
		const rollData = action.getRollData();
		const dcOffFormula = action.save.dc || '0';
		const dcOff = RollPF.safeRollSync(dcOffFormula, rollData);
		if (dcOff.total !== 0) {
			const dc = action.getDC(rollData);
			const formula = o.item.spellbook.baseDCFormula + ' + ' + dcOffFormula;
			const dcHint = Hint.create(i18n.get('PF1.DCThreshold', { threshold: dc }), ['dc'], { hint: formula });
			hints.append(dcHint.element(false));
		}
	}
};
