/*
 * Combined handler for many item specific details.
 *
 * - Broken
 * - Masterwork
 * - Too low item CL
 * - Size
 * - Enhancement bonus
 * - Aura
 */

import { CFG } from '../config.mjs';
import { Hint } from '../hint.mjs';
import { descriptorIcons } from '../common.mjs';
import { i18n } from '../i18n.mjs';

const descriptors = {
	masterwork: new Hint('PF1.Masterwork', ['masterwork'], { icon: 'fas fa-gem' }),
	broken: new Hint('PF1.Broken', ['broken'], { icon: 'fas fa-heart-broken' }),
	unidentified: new Hint('PF1.Unidentified', ['unidentified'], { icon: 'fas fa-eye-slash' }),
};

Hooks.once('ready', () => {
	for (const value of Object.values(descriptors))
		value.label = i18n.get(value.label);
});

const sizeMap = { fine: 0, dim: 1, tiny: 2, sm: 3, med: 4, lg: 5, huge: 6, grg: 7, col: 8 };
const sizeMapInvert = foundry.utils.invertObject(sizeMap);

const auraIconsGI = {
	abj: 'img/game-icons/magic-shield.svg',
	con: 'img/game-icons/portal.svg',
	div: 'img/game-icons/crystal-ball.svg',
	enc: 'img/game-icons/brain-tentacle.svg',
	evo: 'img/game-icons/energy-arrow.svg',
	ill: 'img/game-icons/magic-trick.svg',
	nec: 'img/game-icons/death-juice.svg',
	trs: 'img/game-icons/frog-prince.svg',
	uni: 'img/game-icons/sparkles.svg',
};

const auraIconsTh = {
	abj: 'img/thassilonian/envy.svg',
	con: 'img/thassilonian/sloth.svg',
	div: 'img/thassilonian/truth.svg',
	enc: 'img/thassilonian/lust.svg',
	evo: 'img/thassilonian/wrath.svg',
	ill: 'img/thassilonian/pride.svg',
	nec: 'img/thassilonian/gluttony.svg',
	trs: 'img/thassilonian/greed.svg',
	uni: 'img/thassilonian/diligence.svg',
};

const auraColors = {
	abj: 'green',
	con: 'blue',
	div: '??',
	enc: 'red',
	evo: 'orange',
	ill: 'violet',
	nec: 'indigo',
	trs: 'yellow',
	uni: '??',
};

function getAuraIcon(school, type) {
	const a = type === 'runes' ? auraIconsTh : auraIconsGI;
	return `modules/${CFG.id}/${a[school] ?? a.uni}`;
}

/**
 * @param {import('../handler-options.mjs').HandlerOptions} o Options
 * @param hints
 */
export function handleItem(o, hints) {
	const item = o.item;

	const itemData = o.itemData;

	if (!o.homebrew && o.sanity && o.showSecrets && o.casterLevel > 0 && o.enhancement > 0) {
		const clDiff = o.casterLevel - o.enhancement * 3;
		// Celestial Armor is core item that defies this base logic
		if (clDiff < 0) { // higher CL is possible, lower is odd
			const underLevel = descriptorIcons.inconsistency.clone();
			underLevel.hint = i18n.get('Koboldworks.Sanity.InsufficientCL', { diff: clDiff });
			hints.append(underLevel.element(true));
		}
	}

	const { value: currentHp, max: maxHp } = itemData.hp ?? {};
	if (itemData.broken) {
		const h = descriptors.broken.clone();
		if (maxHp > 0) h.hint = `${h.label}<br>${currentHp}/${maxHp} HP`;
		hints.append(h.element(o.iconize));
	}
	else if (currentHp < maxHp) {
		const h = new Hint(`${currentHp}/${maxHp} HP`, ['damaged'], { icon: 'fa-solid fa-heart-pulse' });
		hints.append(h.element(o.iconize));
	}

	if (itemData.masterwork && !(o.enhancement > 0 && o.showSecrets))
		hints.append(descriptors.masterwork.clone().element(o.iconize));

	if (o.showSecrets) {
		if (o.enhancement > 0)
			hints.append(Hint.create(`+${o.enhancement}`, ['enhancement'], { hint: i18n.get('Koboldworks.Hints.Enhancement', { enh: o.enhancement }) }).element());

		if (!o.isIdentified) hints.append(descriptors.unidentified.clone().element(o.iconize));

		if (o.casterLevel > 0) {
			if (game.settings.get(CFG.id, CFG.SETTING.aura)) {
				const auraKey = itemData.aura.school,
					aura = itemData.aura.custom ? auraKey : pf1.config.spellSchools[auraKey];

				const hint = Hint.create(`${aura} (${o.casterLevel})`, ['aura']);
				const auraD = game.settings.get(CFG.id, CFG.SETTING.schoolAura);
				const asKeys = ['letters', 'letterswcl'].includes(auraD);
				if (o.iconize) {
					if (asKeys) {
						hint.hint = hint.label;
						hint.label = auraKey.length <= 3 ? i18n.get(`Koboldworks.School.${auraKey || 'Unknown'}`) : i18n.get('Koboldworks.School.odd');
						if (auraD === 'letterswcl') hint.label += ` (${o.casterLevel})`;
					}
					else {
						hint.image = getAuraIcon(auraKey, auraD);
					}
				}
				hints.append(hint.element(!asKeys ? o.iconize : false));
			}

			// Add glowing label (faint, moderate, strong)
			const glowLevel = Math.clamp(Math.ceil((o.casterLevel + 1) / 6), 1, 3); // 1-3

			o.element.querySelector('h4').classList.add(`enhancement-${glowLevel}`);
		}
	}

	if (!itemData.resizing) {
		if (!['loot', 'attack', 'consumable'].includes(item.type)) {
			const itemSize = sizeMap[itemData.size || 'med'];
			const actorSize = o.actorData?.traits?.size?.value;
			if (itemSize && actorSize !== itemSize) {
				// const size = Number.isFinite(sizeString) ? sizeString : sizeMap[sizeString];
				const sizeLabel = pf1.config.actorSizes[sizeMapInvert[itemSize]];
				hints.append(Hint.create(sizeLabel.capitalize(), ['size']).element());
			}
		}
	}
}
