import { CFG } from '../config.mjs';
import { Hint } from '../hint.mjs';
import { getHint } from '../common.mjs';

export async function handleCustomHints(o, hints) {
	try {
		if (o.actor?.sheet?.__modItemHintsDisableCustom) return;

		let customHints = getHint(o.item);

		if (customHints === undefined) return;
		if (typeof customHints !== 'string') customHints = customHints.toString();
		// TODO: Add support for custom class tags.
		for (const h of customHints.split(';')) {
			const [label, hint, instructions] = h.split('|', 3);
			const css = ['custom'];
			if (instructions) {
				const re = instructions.match(/css=(?<css>.*)/);
				if (re?.groups.css) css.push(...re.groups.css.split(/\s+/));
			}
			try {
				hints.append(
					Hint.create(
						await pf1.utils.enrichHTMLUnrolled(label.trim(), { rollData: o.rollData, async: true }),
						css,
						{ hint: hint?.trim() ?? null },
					).element(false),
				);
			}
			catch (err) {
				console.error(err, { label, hint, instructions }, o);
			}
		}
	}
	catch (err) {
		console.warn(`ITEM HINTS | Custom | ${o.actor?.name} [${o.actor.id}] | ${o.item.name}:`, o.item);
		console.error(err);
		if (o.actor) {
			console.debug('ITEM HINTS | Custom | Error detected; Temporarily disabling for actor:', o.actor.name);
			if (o.actor.sheet)
				o.actor.sheet.__modItemHintsDisableCustom = true; // Not meant to be stored in database
		}
	}
}
