// Class

import { Hint } from '../hint.mjs';
import { descriptorIcons } from '../common.mjs';
import { i18n } from '../i18n.mjs';

/**
 * @param {HandlerOptions} o Options
 * @param hints
 */
export function handleClass(o, hints) {
	const itemData = o.itemData,
		hp = itemData.fc.hp.value,
		skill = itemData.fc.skill.value,
		alt = itemData.fc.alt.value;
	if (hp > 0) hints.append(Hint.create(`${i18n.get('PF1.FavouredClassBonus.HP')} ×${hp}`, ['fcb', 'hp']).element());
	if (skill > 0) hints.append(Hint.create(`${i18n.get('PF1.FavouredClassBonus.Skill')} ×${skill}`, ['fcb', 'skill']).element());
	if (alt > 0) hints.append(Hint.create(`${i18n.get('PF1.FavouredClassBonus.Alt')} ×${alt}`, ['fcb', 'alt']).element());

	if (o.sanity) {
		const total = hp + skill + alt;
		if (total > 0 && itemData.level !== total) {
			const shint = descriptorIcons.inconsistency.clone();
			shint.hint = i18n.get('Koboldworks.Sanity.FCBInequal', { diff: total - itemData.level });
			shint.additionalClasses.push('fcb');
			hints.append(shint.element(true));
		}
	}
}
