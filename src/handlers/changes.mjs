// Changes

import { CFG } from '../config.mjs';
import { Hint } from '../hint.mjs';
import { i18n } from '../i18n.mjs';

/**
 * Remove flairs, remove excessive spacing, simplify math
 *
 * @param {string} formula
 */
export const stripFlair = (formula) => formula
	.replace(/\[.*?]/g, '').replace(/\s{2,}/g, ' ').replace(/\+ -/g, '-');

/**
 * @param {string} formula
 * @param {object} rollData
 * @returns {number}
 */
function staticRoll(formula, rollData) {
	/* global RollPF */
	const f = RollPF.replaceFormulaData(formula, rollData, { missing: 0, warn: false });
	return RollPF.safeEval(stripFlair(f));
}

// Alter default translations to be shorter
const coreLang = {
	bab: 'PF1.BABAbbr',
	// ac: 'PF1.BuffTarAC', // Generic AC -> AC
	flySpeed: 'PF1.Movement.Mode.fly',
	burrowSpeed: 'PF1.Movement.Mode.burrow',
	landSpeed: 'PF1.Movement.Mode.land',
	climbSpeed: 'PF1.Movement.Mode.climb',
	swimSpeed: 'PF1.Movement.Mode.swim',
	wdamage: CONFIG.Item.typeLabels.weapon,
	sdamage: CONFIG.Item.typeLabels.spell,
	cmb: 'PF1.CMBAbbr',
	cmd: 'PF1.CMDAbbr',
	allSpeeds: 'PF1.All',
	attack: 'PF1.All',
	mattack: 'PF1.Melee',
	rattack: 'PF1.Ranged',
	damage: 'PF1.All',
	spellEffect: 'TYPES.Item.spell',
	allSavingThrows: 'Koboldworks.Short.AllSaves',
	mhp: 'PF1.HPShort',
	str: 'PF1.AbilityShortStr',
	dex: 'PF1.AbilityShortDex',
	con: 'PF1.AbilityShortCon',
	int: 'PF1.AbilityShortInt',
	wis: 'PF1.AbilityShortWis',
	cha: 'PF1.AbilityShortCha',
	mwdamage: 'Koboldworks.Short.MeleeWeapon',
	rwdamage: 'Koboldworks.Short.RangedWeapon',
	twdamage: 'Koboldworks.Short.ThrownWeapon',
	mdamage: 'Koboldworks.Short.Melee',
	rdamage: 'Koboldworks.Short.Ranged',
	ndamage: 'Koboldworks.Short.Natural',
	strMod: 'Koboldworks.Short.StrMod',
	dexMod: 'Koboldworks.Short.DexMod',
	conMod: 'Koboldworks.Short.ConMod',
	intMod: 'Koboldworks.Short.IntMod',
	wisMod: 'Koboldworks.Short.WisMod',
	chaMod: 'Koboldworks.Short.ChaMod',
	bonusSkillRanks: 'Koboldworks.Short.SkillRanks',
	carryMult: 'Koboldworks.Short.CarryMult',
};

// Add extensions
const moduleLangExtensions = {
	nac: 'Koboldworks.Short.NaturalAC',
	tac: 'Koboldworks.Short.TouchAC',
	mDexA: 'Koboldworks.Short.ArmorMaxDex',
	mDexS: 'Koboldworks.Short.ShieldMaxDex',
	ffac: 'Koboldworks.Short.FlatfootedAC',
	ffcmd: 'Koboldworks.Short.FlatfootedCMD',
	spellResist: 'Koboldworks.Short.SpellResistance',
	allChecks: 'Koboldworks.Short.AllAblChecks',
	// Ability score stuff
	strSkills: 'Koboldworks.Short.StrSkills',
	dexSkills: 'Koboldworks.Short.DexSkills',
	conSkills: 'Koboldworks.Short.ConSkills',
	intSkills: 'Koboldworks.Short.IntSkills',
	wisSkills: 'Koboldworks.Short.WisSkills',
	chaSkills: 'Koboldworks.Short.ChaSkills',
	strChecks: 'Koboldworks.Short.StrChecks',
	dexChecks: 'Koboldworks.Short.DexChecks',
	conChecks: 'Koboldworks.Short.ConChecks',
	intChecks: 'Koboldworks.Short.IntChecks',
	wisChecks: 'Koboldworks.Short.WisChecks',
	chaChecks: 'Koboldworks.Short.ChaChecks',
	init: 'Koboldworks.Short.Initiative',
	ref: 'Koboldworks.Short.Reflex',
	fort: 'Koboldworks.Short.Fortitude',
	will: 'Koboldworks.Short.Will',
};

const changesRequiringLabel = [
	// 'allChecks',
	// 'allSavingThrows'
];

const disableUnlang = () => game.i18n.lang !== 'en' && !game.settings.get(CFG.id, CFG.SETTING.unlang);

class ChangeLabel {
	label;
	optional;
	symbolic;
	constructor(label, { optional = true, symbolic = true } = {}) {
		this.label = label;
		this.optional = optional;
		this.symbolic = symbolic;
	}
}

/**
 * @param {string} k
 * @param {import('../handler-options.mjs').HandlerOptions} o Options
 * @returns {ChangeLabel}
 */
function generateChangeLabel(k, o) {
	switch (k) {
		case 'ability': case 'abilityChecks': return new ChangeLabel(i18n.get('Koboldworks.Short.Ability'));
		case 'ac': throw new Error('AC'); // outdated?
		case 'cmd': throw new Error('CMD');
		case 'attack': case 'attacks': return new ChangeLabel(i18n.get('Koboldworks.Short.Attack'), { optional: false });
		case 'damage': return new ChangeLabel(i18n.get('Koboldworks.Short.Damage'), { optional: false });
		case 'savingThrows': return new ChangeLabel(i18n.get('Koboldworks.Short.SavingThrow'));
		case 'skill': case 'skills': return new ChangeLabel(i18n.get('Koboldworks.Short.Skill'));
		case 'speed': return new ChangeLabel(i18n.get('Koboldworks.Short.Speed'), { optional: false });
		case 'spell': return new ChangeLabel(i18n.get('Koboldworks.Short.Spell'));
		case 'misc': return new ChangeLabel(i18n.get('Koboldworks.Short.Misc'));
		case 'health': return new ChangeLabel(i18n.get('Koboldworks.Short.Health'));
		case 'defense': return new ChangeLabel(i18n.get('Koboldworks.Short.Defense'));
		case 'dc': return new ChangeLabel(i18n.get('Koboldworks.Short.SpellDC'));
		default:
			try {
				const t = pf1.config.buffTargets[k] ?? pf1.config.contextNoteTargets[k];
				if (t) {
					return new ChangeLabel(`~${t.category.capitalize()}`, { optional: false });
				}
				else {
					const cat = pf1.config.buffTargetCategories[k];
					if (cat !== undefined) return new ChangeLabel(cat?.label, { optional: false });
				}
			}
			catch (err) {
				console.warn('Item Hints | Unrecognized key:', k, 'in', o.item.name, err);
				return new ChangeLabel(i18n.get('Koboldworks.Generic.BadSource'), { optional: false, symbolic: false });
			}
	}
	return new ChangeLabel(i18n.get('Koboldworks.Generic.UnknownSource'), { optional: false, symbolic: false });
};

// Combine
let simplifications = {};
Hooks.once('init', () => simplifications = Object.assign({}, coreLang, disableUnlang() ? {} : moduleLangExtensions));

class UniformObject {
	/** @type {string} */
	target;
	/** @type {boolean} */
	isChange;
	/** @type {string} */
	formula;
	/** @type {object} */
	change;
	/** @type {ItemPF} */
	item;
	/** @type {ActorPF} */
	actor;
	constructor(ch, actor = null, item = null) {
		this.target = ch.target;
		this.isChange = ch instanceof pf1.components.ItemChange;
		this.formula = ch.formula;
		this.change = ch;
		this.actor = actor;
		this.item = item;
	}

	#source;
	get source() {
		if (!this.#source) {
			const sources = this.isChange ? pf1.config.buffTargets : pf1.config.contextNoteTargets;
			this.#source = sources[this.target];
			if (!this.#source) {
				if (this.isSkill) this.#source = sources.skills;
				else if (this.isConcentration) this.#source = sources.concentration;
				else if (this.isCL) this.#source = sources.cl;
				else if (this.isSpellDC) this.#source = sources.dc;
				else console.error('No Source', this);
			}
		}
		return this.#source;
	}

	/** @type {boolean} */
	get isCL() { return this.target.startsWith('cl.'); }

	/** @type {boolean} */
	get isSpellDC() { return this.target.startsWith('dc.'); }

	/** @type {boolean} */
	get isConcentration() { return this.target.startsWith('concn.'); }

	/** @type {boolean} */
	get isSkill() { return this.target.startsWith('skill.'); }

	#key;
	/** @type {string} */
	get key() {
		if (!this.#key) {
			this.#key = this.source?.category;
			if (this.isSkill) this.#key = 'skill';
			// HACK: fix for context notes with DC target
			else if (this.isSpellDC) this.#key ??= 'dc';
			if (!this.#key) console.error('No Key', this);
		}
		return this.#key;
	}

	#skill;
	_skillId;
	get skill() {
		if (this.isSkill && this.#skill === undefined) {
			const re = this.target.match(/^skill\.(?<id>[\w.]+)/);
			this._skillId = re?.groups.id;
			try {
				this.#skill = this.actor.getSkillInfo(this._skillId);
			}
			catch (err) {
				// Ignore
				this.#skill = null;
			}
		}
		return this.#skill;
	}

	#label;
	get label() {
		this.#label ??= this._generateLabel();
		return this.#label;
	}

	_generateLabel() {
		if (this.isSkill) return this.#skill?.name;
		if (this.isCL) return i18n.get('PF1.CasterLevelAbbr');
		if (this.isConcentration) return i18n.get('Koboldworks.Missing.PF1.ConcentrationAbbr');
		if (this.isSpellDC) return i18n.get('PF1.SpellDC');
		if (this.target in simplifications) return i18n.get(simplifications[this.target]);
		return (this.isChange ? pf1.config.buffTargets : pf1.config.contextNoteTargets)[this.target]?.label ?? this.target;
	}
}

/**
 * @param {import('../handler-options.mjs').HandlerOptions} o Options
 * @param hints
 */
export function handleChanges(o, hints) {
	if (!o.changes) return;
	if (!o.isIdentified) return;
	const itemData = o.itemData;
	if (!(itemData.changes?.length > 0 || itemData.contextNotes?.length > 0)) return;

	const commonTargets = {}, problems = {};

	/**
	 * @param {object} obj Collective object.
	 * @param {pf1.components.ItemChange|pf1.components.ContextNote} ch Current change
	 * @param {ItemPF} item Item instance.
	 * @returns
	 */
	const createUniformObject = (obj, ch, item) => {
		const isChange = ch instanceof pf1.components.ItemChange;
		let cto;
		try {
			cto = new UniformObject(ch, o.actor, item);

			// Check for missing target
			// No longer needed
			if (!(cto.target?.length > 0)) {
				if (!('noTarget' in problems)) {
					console.warn('ITEM HINTS | Missing Target | Item:', item.name, '; Change:', ch, item);
					problems['noTarget'] = {
						label: i18n.get('Koboldworks.Sanity.Change.TargetlessLabel'),
						hint: i18n.get('Koboldworks.Sanity.Change.TargetlessHint'),
					};
				}
				return obj;
			}

			// Check for missing target
			if (cto.label === ch.target) {
				if (!('badTarget' in problems)) {
					console.warn('ITEM HINTS | Bad Target |', item.name, ch, item);
					problems['badTarget'] = {
						label: i18n.get('Koboldworks.Sanity.Change.BadTargetLabel'),
						hint: i18n.get('Koboldworks.Sanity.Change.BadTargetHint', { target: ch.target }),
					};
				}
				return obj;
			}

			// Check for missing formula
			if (isChange && !(ch.formula?.length > 0)) {
				if (!('noFormula' in problems)) {
					// Change specific
					console.warn('ITEM HINTS | Missing Formula | Item:', item.name, '; Change:', ch, item);
					problems['noFormula'] = {
						label: i18n.get('Koboldworks.Sanity.Change.FormulalessLabel'),
						hint: i18n.get('Koboldworks.Sanity.Change.FormulalessHint'),
					};
				}
				return obj;
			}

			// Context note missing text
			if (!isChange && !(ch.text?.trim().length > 0)) {
				if (!('noText' in problems)) {
					// Context note specific
					console.warn('ITEM HINTS | Missing Text |', item.name, ch, item);
					problems['noText'] = {
						label: i18n.get('Koboldworks.Sanity.Change.TextlessLabel'),
						hint: i18n.get('Koboldworks.Sanity.Change.TextlessHint', { target: ch.target }),
					};
				}
				return obj;
			}

			// Missing custom/sub skill
			if (cto.isSkill && !cto.skill) {
				if (!('badSkill' in problems)) {
					problems['badSkill'] = {
						label: i18n.get('Koboldworks.Sanity.Change.MissingSkillLabel'),
						hint: i18n.get('Koboldworks.Sanity.Change.MissingSkillHint', { skillId: cto._skillId }),
					};
				}
				return obj;
			}

			// Store UO
			const key = cto.key;
			if (key) {
				obj[key] ??= {};
				obj[key][cto.target] ??= cto;
				commonTargets[key] ??= {};
				commonTargets[key][cto.target] ??= cto;
				return obj;
			}
			else {
				console.error('ITEM HINTS | No Key', { obj, ch, item, cto });
			}
		}
		catch (err) {
			console.error('ITEM HINTS | Error:', err, { change: ch, object: cto, item, uniform: obj });
			return obj;
		}
	};

	const transformChanges = (list) => list?.reduce((obj, ch) => createUniformObject(obj, ch, o.item), {}) ?? {};
	const ctargets = transformChanges(o.item.changes),
		ntargets = transformChanges(itemData.contextNotes);

	const maxLabelLength = game.settings.get(CFG.id, CFG.SETTING.maxLabelLength);

	/**
	 * @param {string} key
	 * @param {boolean} isChange
	 * @returns {string}
	 */
	function formatLabel(key, isChange = true) {
		const ct = commonTargets[key],
			ck = Object.keys(ct),
			b = ck.filter(x => ct[x].isChange === isChange).map(x => ct[x].label),
			lb = generateChangeLabel(key, o),
			sym = lb.symbolic ? '±' : '';

		if (b.length === 0) return `${sym}${lb.label}`;
		const label = [sym];
		let sublabel;
		if (lb.optional && !ck.some(r => changesRequiringLabel.indexOf(r) >= 0)) {
			sublabel = b.join(';');
		}
		else {
			label.push(lb.label, ':');
			sublabel = b.join(',');
		}
		if (sublabel?.length > maxLabelLength) sublabel = i18n.get('Koboldworks.Hints.Many');
		label.push(sublabel);
		return label.join('');
	};

	const makeChangeHint = (key) => {
		// Changes & Notes
		const cC = [], cN = [];

		Object.keys(commonTargets[key])
			.forEach(x => {
				if (ctargets[key]?.[x]) {
					const cto = ctargets[key][x];
					if (!cto.isChange) return;
					let total;
					try {
						total = staticRoll(cto.change.formula, o.rollData);
					}
					catch (err) {
						/* ignore */
						return;
					}
					const label = [cto.label, ' = ', total !== undefined ? `~${total}` : cto.change.formula];
					cC.push(label.join(''));
				}
				if (ntargets[key]?.[x]) {
					const cto = ntargets[key][x];
					if (cto.isChange) return;
					cN.push(cto.label);
				}
			});

		let changeHint, noteHint;
		if (cC.length > 0) {
			const title = i18n.get(cC.length == 1 ? 'PF1.Change' : 'PF1.Changes') + ': ';
			cC.unshift(title);
			const sep = cC.length == 2 ? '' : '\n';
			changeHint = Hint.create(formatLabel(key, true), ['change'], { hint: cC.join(sep) });
		}
		if (cN.length > 0) {
			const title = i18n.get(cN.length == 1 ? 'Koboldworks.Hints.Contextual' : 'PF1.ContextNotes') + ': ';
			cN.unshift(title);
			const sep = cN.length == 2 ? '' : '\n';
			noteHint = Hint.create(formatLabel(key, false), ['contextual'], { hint: cN.join(sep) });
		}

		return [changeHint, noteHint];
	};

	Object.keys(commonTargets)
		.forEach(ch => {
			const [changes, notes] = makeChangeHint(ch);
			if (changes) hints.append(changes.element());
			if (notes) hints.append(notes.element());
		});

	Object.values(problems).forEach(problem => hints.prepend(
		Hint.create(problem.label, ['change', 'error'], { hint: problem.hint }).element(),
	));
}
