/*
 * Item value.
 */

import { descriptorIcons } from '../common.mjs';
import { i18n } from '../i18n.mjs';

/**
 * @param {HandlerOptions} o Options
 * @param hints
 * @param inContainer
 */
export function handleValue(o, hints, inContainer) {
	if (o.values === 'none') return;
	const item = o.item,
		itemData = o.itemData,
		quantity = itemData.quantity;
	if (!(quantity > 0)) return;

	const isContainer = item.type === 'container',
		isTradeGoods = itemData.subType === 'tradeGoods';

	let showValue = false;
	switch (o.values) {
		case 'all':
			showValue = true;
		// eslint-disable-next-line no-fallthrough
		case 'expanded':
			if (inContainer) showValue = true;
		// eslint-disable-next-line no-fallthrough
		case 'containers':
			if (isContainer) showValue = true;
		// eslint-disable-next-line no-fallthrough
		case 'tradegoods':
			if (isTradeGoods) showValue = true;
			break;
		case 'none':
			return;
	}

	if (!showValue) return;

	const tCost = descriptorIcons.value.clone(),
		cpSelf = item.getValue({ sellValue: 1, recursive: false, single: true, inLowestDenomination: true }),
		cpTotal = item.getValue({ sellValue: 1, recursive: true, inLowestDenomination: true });

	const { standard, rate: rates } = pf1.config.currency;
	const rate = rates[standard] ?? 1;

	const formatNumber = Intl.NumberFormat().format;
	const gpNum = (n) => formatNumber(n / rate);

	const gpStr = `PF1.Currency.Inline.${standard}`;

	const gpLabel = i18n.get(gpStr, { value: gpNum(cpTotal) });
	tCost.label = gpLabel;

	if (isContainer) {
		// const currency = item.getTotalCurrency();
		tCost.hint = i18n.get('Koboldworks.Hints.ContentsValue',
			{
				value: i18n.get(gpStr, { value: gpNum(cpSelf) }),
				contents: i18n.get(gpStr, { value: gpNum(cpTotal - cpSelf) }),
			});
	}
	else {
		tCost.hint = quantity > 1 ? i18n.get('Koboldworks.Hints.ValueMult', {
			value: gpNum(cpSelf),
			quantity,
			total: gpLabel,
		}) : gpLabel;
	}

	hints.append(tCost.element(o.iconize));
}
