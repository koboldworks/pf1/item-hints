import { Hint } from '../hint.mjs';

/**
 * @param {HandlerOptions} o
 * @param {Hint[]} hints
 */
export function handleDisabled(o, hints) {
	if (['buff', 'class', 'spell', 'race'].includes(o.item.type)) return;

	if (o.item.isPhysical) {
		if (o.itemData.quantity === 0) {
			//
		}
	}

	if (!o.item.isActive) {
		// Inactive
	}

	if (o.item.isCharged && o.item.charges === 0) {
		// Out of charges
	}

	// Do hint
}
