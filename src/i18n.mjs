export class i18n {
	/**
	 * Tests if translation key exists.
	 *
	 * @param {string} string
	 * @returns {boolean} - Whether the i18n string exists.
	 */
	static has = (string) => game.i18n.has(string);

	/**
	 * @param {string} string i18n string path
	 * @param {Record<string,string>} [mapping] - Mapping of keys to strings
	 * @returns {string} - Localized string
	 */
	static get = (string, mapping) => mapping ? game.i18n.format(string, mapping) : game.i18n.localize(string);
}
